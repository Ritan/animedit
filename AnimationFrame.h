#pragma once
#include "AnimationNode.h"
#include <QMap>
#include "Animation.h"

class AnimationFrame
{
public:
	AnimationFrame( Animation* );
	~AnimationFrame();
	void drawFrame( QPainter& p );
	void hideFrame();
	void saveNodePositions();
	void loadNodePositions();
	void onNodeDeletion( unsigned int node );

	friend QDataStream& operator<<( QDataStream& stream, AnimationFrame& node );
	friend QDataStream& operator>>( QDataStream& stream, AnimationFrame& node );

	QMap<unsigned int, QPoint>& getNodesPositions()
	{
		return m_nodePositions;
	}

private:
	unsigned int  lastNodeID;
	Animation* m_parentAnim;
	QMap<unsigned int, QPoint> m_nodePositions;
};

