#ifndef ANIMEDIT_H
#define ANIMEDIT_H

#include <QtWidgets/QMainWindow>
#include "ui_animedit.h"
#include <QPushButton>
#include <QLayout>
#include <QWidget>
#include <QtCore>

class Animation;
class AnimationNode;

class AnimEdit : public QMainWindow
{
	Q_OBJECT

public:
	AnimEdit( QWidget* parent = 0 );
	~AnimEdit();

private:
	Ui::AnimEditClass ui;
	Animation* anim;
protected:
	virtual void mousePressEvent( QMouseEvent* event );
	virtual void paintEvent( QPaintEvent* );

public slots:
	void on_actionSave_triggered(bool);
	void on_actionLoad_triggered(bool);
	void on_actionRemove_selected_triggered(bool);
	void on_actionNext_frame_triggered(bool);
	void on_actionQuit_triggered(bool);
	void on_actionPrevious_frame_triggered(bool);
	void on_actionHelp_triggered(bool);
};

#endif // ANIMEDIT_H
