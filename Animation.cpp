#include "Animation.h"
#include "AnimationFrame.h"
#include "AnimationNode.h"
#include "QFile"

Animation::Animation( QWidget* host ) : m_animationDrawHost( host )
{
	nodeImg = new QPixmap(":/images/node.png");
	selNodeImg = new QPixmap(":/images/selNode.png");
	auto frame = new AnimationFrame( this );
	m_frameList.append(frame);
	
}

Animation::Animation(QWidget* host, const QString& filename) : m_animationDrawHost( host )
{
	nodeImg = new QPixmap(":/images/node.png");
	selNodeImg = new QPixmap(":/images/selNode.png");
	loadFromFile(filename);
}

Animation::~Animation()
{
	for ( auto nodeID : m_nodeMap.keys() )
	{
		delete m_nodeMap[nodeID];
	}

	for ( auto& frame : m_frameList )
	{
		delete frame;
	}

	m_nodeMap.clear();
	m_frameList.clear();
}

void Animation::nextFrame()
{
	getCurrentFrame()->hideFrame();
	getCurrentFrame()->saveNodePositions();

	if ( frameIndex < m_frameList.count() - 1 )
	{
		frameIndex++;
	}
	else
	{
		//I dont like it, but the reqs say so
		createNewFrame();
	}

	getCurrentFrame()->loadNodePositions();
	m_animationDrawHost->repaint();
}

void Animation::prevFrame()
{
	getCurrentFrame()->hideFrame();
	getCurrentFrame()->saveNodePositions();
	if ( frameIndex > 0 )
	{
		frameIndex--;
	}
	getCurrentFrame()->loadNodePositions();
	m_animationDrawHost->repaint();
}

AnimationFrame* Animation::getCurrentFrame()
{
	return m_frameList[frameIndex];
}

void Animation::createNewFrame()
{
	if ( !m_frameList.empty() )
	{
		getCurrentFrame()->saveNodePositions();
		auto frame = new AnimationFrame(*getCurrentFrame());
		m_frameList.append(frame);
		frameIndex = m_frameList.count() - 1;
	}
}

unsigned int Animation::createNewNode( QPoint pos )
{
	auto node = new AnimationNode( m_animationDrawHost, this );
	node->centeredMove( pos );
	node->setAnimationID( m_nodeCount++ );
	node->setPixmap( *nodeImg );

	m_nodeMap[node->ID()] = node;

	for ( auto& frame : m_frameList )
	{
		frame->getNodesPositions()[node->ID()] = pos;
	}

	return node->ID();
}

void Animation::addExistingNode( AnimationNode* node )
{
	m_nodeMap[node->ID()] = node;
	node->setPixmap( *nodeImg );

	if ( node->ID() >= m_nodeCount )
		m_nodeCount = node->ID() + 1;
}

QMap<unsigned int, AnimationNode*>& Animation::getNodeMap()
{
	return m_nodeMap;
}

bool Animation::isLastFrame()
{
	return frameIndex == m_frameList.count() - 1;
}

void Animation::onNodeDelete( unsigned int nodeID )
{
	auto node = getNodeMap()[nodeID];

	if ( selectedNodeID == nodeID )
	{
		resetSelection();
	}

	for ( auto& frame : m_frameList )
	{
		frame->onNodeDeletion( nodeID );
	}

	m_nodeMap.remove( node->ID() );
}

void Animation::updateSelectedNode( unsigned int nodeID )
{
	auto node = getNodeMap()[nodeID];

	if ( selectedNode() == node )
	{
		selectedNode()->toggleSelected( false );
	}

	if ( node )
	{
		node->toggleSelected( true );
		haveSelection = true;
	}
	else
	{
		haveSelection = false;
	}

	selectedNodeID = nodeID;
}

void Animation::resetSelection()
{
	if ( haveSelection )
	{
		selectedNode()->toggleSelected( false );
		haveSelection = false;
		selectedNodeID = 0;
	}
}

void Animation::drawCurrentFrame( QPainter& p )
{
	getCurrentFrame()->drawFrame( p );
}

void Animation::saveToFile( const QString& filename )
{
	QFile file( filename );
	file.open( QIODevice::WriteOnly );
	QDataStream stream( &file );

	stream << 0xBEAFu;
	file.flush();
	stream << m_nodeMap.count();
	file.flush();

	for ( auto& node : m_nodeMap )
	{
		stream << *node;
	}

	stream << m_frameList.count();
	for ( auto& frame : m_frameList )
	{
		stream << *frame;
	}
}

void Animation::loadFromFile( const QString& filename )
{
	QFile file( filename );
	file.open( QIODevice::ReadOnly );
	QDataStream stream( &file );

	unsigned int magick;
	stream >> magick;
	if ( magick != 0xBEAFu )
	{
		return;
	}

	int count;
	stream >> count;

	for ( int i = 0; i < count; i++ )
	{
		auto node = new AnimationNode( m_animationDrawHost, this );
		stream >> *node;
		addExistingNode( node );
	}

	m_frameList.clear();

	int frameCount;
	stream >> frameCount;

	for ( int i = 0; i < frameCount; i++ )
	{
		AnimationFrame* frame = new AnimationFrame( this );
		stream >> *frame;
		m_frameList.append( frame );
	}

	frameIndex = 0;
	getCurrentFrame()->loadNodePositions();
}

const QRect Animation::getNodePixmapRect()
{
	return nodeImg->rect();
}

QPixmap& Animation::nodePixmap()
{
	return *nodeImg;
}

QPixmap& Animation::selectedNodePixmap()
{
	return *selNodeImg;
}


QDataStream& operator<<( QDataStream& stream, AnimationNode& node )
{
	stream << node.m_nodeId<<node.connectedNodes;
	return stream;
}

QDataStream& operator>>( QDataStream& stream, AnimationNode& node )
{
	unsigned int id;
	QList<unsigned int> connections;
	stream >> id >> connections;
	node.setAnimationID( id );
	node.connectedNodes = connections;
	return stream;
}

QDataStream& operator<<( QDataStream& stream, AnimationFrame& frame )
{
	for ( auto& key : frame.m_nodePositions.keys() )
	{
		stream << key << frame.m_nodePositions[key];
	}
	return stream;
}

QDataStream& operator>>( QDataStream& stream, AnimationFrame& frame )
{
	for ( int i = 0; i < frame.m_parentAnim->getNodeMap().count(); i++ )
	{
		unsigned int id;
		QPoint pos;

		stream >> id >> pos;
		frame.m_nodePositions[id] = pos;
	}
	return stream;
}
