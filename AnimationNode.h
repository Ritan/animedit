#pragma once
#include <QLabel>

class QMenu;
class Animation;

class AnimationNode :
	public QLabel
{
	Q_OBJECT

public:
	AnimationNode( QWidget* parent, Animation* holdingAnimation );
	virtual ~AnimationNode();

	virtual void mouseReleaseEvent( QMouseEvent* ev );
	virtual void mousePressEvent( QMouseEvent* ev );
	virtual void paintEvent( QPaintEvent* );
	virtual void mouseMoveEvent( QMouseEvent* ev );

	void centeredMove( const QPoint& );

	void toggleConnection( unsigned int );
	void addConnection( unsigned int );
	void removeConnection( unsigned int );
	QList<unsigned int> getConnections();

	void drawConnections( QPainter& painter );
	void setAnimationID( unsigned int lastNodeID );

	unsigned int ID()
	{
		return m_nodeId;
	}

	QPoint& centerPos();

	friend QDataStream& operator<<( QDataStream& stream, AnimationNode& node );
	friend QDataStream& operator>>( QDataStream& stream, AnimationNode& node );

signals:
	void clicked( unsigned int, QMouseEvent* );
	void onDeletion( unsigned int );

public slots:
	void onConnectedNodeDeletion( unsigned int nodeID );
	void toggleSelected( bool selected );
private:
	unsigned int m_nodeId;
	QList<unsigned int> connectedNodes;
	QPoint m_center;

	static unsigned int lastNodeId;
	bool followingMode;
	bool isNodeMoved;

	Animation* m_parentAnim;
	QPoint clickPosition;
	QRect boundingRect;
};

