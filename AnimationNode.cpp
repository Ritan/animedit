#include "AnimationNode.h"
#include <QMouseEvent>
#include <QPainter>
#include <QAction>
#include <QMenu>
#include "Animation.h"
#include "QMessageBox"

unsigned int AnimationNode::lastNodeId = 0;

AnimationNode::AnimationNode(QWidget* parent, Animation* parentAnim) : QLabel(parent), m_nodeId(lastNodeId), m_parentAnim(parentAnim)
{
	boundingRect = parentAnim->getNodePixmapRect();
	setMask(boundingRect.adjusted(0, 0, 0, 5));;
	
	setAttribute( Qt::WA_NoMousePropagation );
	show();
}



AnimationNode::~AnimationNode()
{
	emit onDeletion( ID() );
	m_parentAnim->onNodeDelete( ID() );
}


void AnimationNode::paintEvent( QPaintEvent* ev )
{
	QLabel::paintEvent( ev );
}

void AnimationNode::drawConnections( QPainter& p )
{
	auto nodeMap = m_parentAnim->getNodeMap();

	if ( !connectedNodes.isEmpty() )
		for ( auto& ID : connectedNodes )
		{
			p.drawLine( m_center, nodeMap[ID]->m_center );
		}
}


void AnimationNode::centeredMove( const QPoint& pos )
{
	boundingRect.moveCenter( pos );
	QLabel::move( boundingRect.topLeft() );
	m_center = pos;
}

void AnimationNode::toggleConnection( unsigned int targetID )
{
	auto nodeMap = m_parentAnim->getNodeMap();

	if ( !connectedNodes.contains( targetID ) )
	{
		addConnection( targetID );
		nodeMap[targetID]->addConnection( ID() );
	}
	else
	{
		removeConnection( targetID );
		nodeMap[targetID]->removeConnection( ID() );
	}

	( ( QWidget* )parent() )->repaint();
}

void AnimationNode::mouseMoveEvent( QMouseEvent* ev )
{
	QWidget* par = ( ( QWidget* )parent() );

	if (ev->buttons() & Qt::LeftButton)
	{
        const QPoint& pos = par->mapFromGlobal(ev->globalPos());
		if ( (( clickPosition - pos ).manhattanLength() > 15)||isNodeMoved )
		{
			centeredMove(pos);
			isNodeMoved = true;
		}
	}
	( ( QWidget* )parent() )->repaint();
}

void AnimationNode::mousePressEvent( QMouseEvent* ev )
{
	if (ev->buttons() & Qt::LeftButton)
	{
		QWidget* par = ((QWidget*)parent());
		clickPosition = par->mapFromGlobal(ev->globalPos());
		isNodeMoved = false;
	}
}

void AnimationNode::mouseReleaseEvent( QMouseEvent* ev )
{
    Q_UNUSED(ev);

	if (!isNodeMoved)
	{
		if (!m_parentAnim->haveSelection)
		{
			m_parentAnim->updateSelectedNode(ID());
		}
		else
		{
			m_parentAnim->selectedNode()->toggleConnection(ID());
			m_parentAnim->resetSelection();
		}
	}
	isNodeMoved = false;
}


void AnimationNode::setAnimationID( const unsigned int id )
{
	m_nodeId = id;
	setText( QString( " %1" ).arg( m_nodeId ) );
}

QPoint& AnimationNode::centerPos()
{
	return m_center;
}

void AnimationNode::onConnectedNodeDeletion( unsigned int nodeID )
{
	connectedNodes.removeOne( nodeID );
}

void AnimationNode::removeConnection( unsigned int targetID )
{
	auto nodeMap = m_parentAnim->getNodeMap();
	disconnect( nodeMap[targetID], SIGNAL( onDeletion( unsigned int ) ), this, SLOT( onConnectedNodeDeletion( unsigned int ) ) );
	connectedNodes.removeOne( targetID );
}

void AnimationNode::addConnection( unsigned int targetID )
{
	auto nodeMap = m_parentAnim->getNodeMap();
	connect( nodeMap[targetID], SIGNAL( onDeletion( unsigned int ) ), this, SLOT( onConnectedNodeDeletion( unsigned int ) ) );
	connectedNodes.append( targetID );
}

void AnimationNode::toggleSelected( bool selected )
{
	if ( selected )
	{
		this->setPixmap(m_parentAnim->selectedNodePixmap());
	}
	else
	{
		this->setPixmap(m_parentAnim->nodePixmap());
	}
}

QList<unsigned int> AnimationNode::getConnections()
{
	return connectedNodes;
}
