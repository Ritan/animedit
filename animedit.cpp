﻿#include "animedit.h"
#include <QMessageBox>
#include <QKeyEvent>
#include <QPainter>
#include <QThread>
#include "QPushButton"
#include <QMenu>
#include <QAction>
#include <QFileDialog>

#include "Animation.h"
#include "AnimationNode.h"
#include "AnimationFrame.h"
#include <string>

AnimEdit::AnimEdit( QWidget* parent )
	: QMainWindow( parent )
{
	ui.setupUi( this );

	anim = new Animation( this );
}

AnimEdit::~AnimEdit()
{

}

void AnimEdit::mousePressEvent( QMouseEvent* event )
{
    anim->createNewNode(event->pos());
	anim->resetSelection();
}


void AnimEdit::paintEvent( QPaintEvent* ev )
{
	QPainter p( this );
	p.setRenderHint( QPainter::Antialiasing );
	p.setPen( QColor( 255, 0, 0 ) );
	p.drawText( QPoint( 0, 30 ), QString( "Frame num: %1" ).arg( anim->getFrameIndex() ) );
	
	QPen pen;
	pen.setWidth(2);
	pen.setColor(QColor(0, 0, 0));
	p.setPen(pen);
	p.setBrush(QColor(0, 255, 0));

	anim->drawCurrentFrame( p );
	QMainWindow::paintEvent( ev );
}

void AnimEdit::on_actionSave_triggered(bool)
{
	QString fileName = QFileDialog::getSaveFileName(this, "Save file", ".", "Animation(*.ane);;All Files (*.*)");

	if (fileName != "")
		anim->saveToFile(fileName);
}

void AnimEdit::on_actionLoad_triggered(bool)
{
	QString filename = QFileDialog::getOpenFileName(this, "Open file", ".", "Animation(*.ane);;All Files (*.*)");
	if (filename != "")
	{
		delete anim;
		anim = new Animation( this, filename );
		repaint();
	}
}

void AnimEdit::on_actionRemove_selected_triggered(bool)
{
	delete anim->selectedNode();
	repaint();
}

void AnimEdit::on_actionNext_frame_triggered(bool)
{
	anim->nextFrame();
}

void AnimEdit::on_actionQuit_triggered(bool)
{
	delete anim;
	this->close();
}

void AnimEdit::on_actionPrevious_frame_triggered(bool)
{
	anim->prevFrame();
}

void AnimEdit::on_actionHelp_triggered(bool)
{
	const wchar_t* helpStr(
		L"<p>AnimEdit Help</p>"
		L"<p>Создание вершины:<br>"
		L"Создать вершину можно кликом мыши в нужном месте</p>"
		L"<p>Соединение вершин:<br>"
		L"Для соединения вершин нужно сначачла выделить первую вершину, после чего кликнуть по второй.</p>"
		L"<p>Удаление связи:<br>"
		L"Для того, чтобы удалить связь между вершинами нужно выбрать одну из связанных вершин и кликнуть по другой.</p>");

	QMessageBox::about(this, tr("About Scribble"), QString::fromWCharArray(helpStr));
}
