SOURCES += \
    Animation.cpp \
    AnimationFrame.cpp \
    AnimationNode.cpp \
    animedit.cpp \
    main.cpp

RESOURCES += \
    animedit.qrc

FORMS += \
    animedit.ui

HEADERS += \
    Animation.h \
    AnimationFrame.h \
    AnimationNode.h \
    animedit.h

QT += widgets

QMAKE_CXXFLAGS += -std=c++11
