#include "AnimationFrame.h"
#include "Animation.h"
#include "QPainter"

AnimationFrame::AnimationFrame( Animation* parentAnim ) : m_parentAnim( parentAnim )
{
}


AnimationFrame::~AnimationFrame()
{
}

void AnimationFrame::loadNodePositions()
{
	auto nodeMap = m_parentAnim->getNodeMap();

	for ( auto& nodeID : nodeMap.keys() )
	{
		nodeMap[nodeID]->centeredMove( m_nodePositions[nodeID] );
	}
}

void AnimationFrame::drawFrame( QPainter& p )
{
	for ( auto& node : m_parentAnim->getNodeMap() )
	{
		node->drawConnections( p );
	}
}

void AnimationFrame::hideFrame()
{
}

void AnimationFrame::saveNodePositions()
{
	auto nodeMap = m_parentAnim->getNodeMap();
	for ( auto& nodeID : nodeMap.keys() )
	{
		m_nodePositions[nodeID] = nodeMap[nodeID]->centerPos();
	}
}

void AnimationFrame::onNodeDeletion( unsigned int nodeID )
{
	m_nodePositions.remove( nodeID );
}
