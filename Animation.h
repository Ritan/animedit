#pragma once
#include <QObject>
#include <QDataStream>
#include "QMap"

class AnimationFrame;
class AnimationNode;
class QPainter;


QDataStream& operator<<( QDataStream& stream, AnimationNode& node );
QDataStream& operator>>( QDataStream&, AnimationNode& node );

QDataStream& operator<<( QDataStream& stream, AnimationFrame& frame );
QDataStream& operator>>( QDataStream&, AnimationFrame& frame );

class Animation : public QObject
{
	Q_OBJECT
public:
	Animation( QWidget* host );
	Animation( QWidget* host, const QString& filename);

	virtual ~Animation();

	void nextFrame();
	void prevFrame();
	bool isLastFrame();
	AnimationFrame* getCurrentFrame();
	void createNewFrame();
	void drawCurrentFrame( QPainter& p );

    unsigned int createNewNode( QPoint pos );
	QMap<unsigned int, AnimationNode*>& getNodeMap();
	void updateSelectedNode( unsigned int );
	void resetSelection();
	void addExistingNode( AnimationNode* node );

	unsigned int selectedNodeID = 0;
	bool haveSelection = false;

	unsigned int getFrameIndex()
	{
		return frameIndex;
	}

	AnimationNode* selectedNode()
	{
		if ( haveSelection )
		{
			return getNodeMap()[selectedNodeID];
		}
		else
		{
			return nullptr;
		}
	}

	void saveToFile( const QString& fileName );
	void loadFromFile( const QString& filename );

    const QRect getNodePixmapRect();

	QPixmap& nodePixmap();
	QPixmap& selectedNodePixmap();

private:
	QList<AnimationFrame*> m_frameList;

	QMap<unsigned int, AnimationNode*> m_nodeMap;

	unsigned int frameIndex = 0;
	unsigned int m_nodeCount = 0;

	QPixmap * nodeImg;
	QPixmap * selNodeImg;

	QWidget* m_animationDrawHost;

public slots:
	void onNodeDelete( unsigned int node );
	
};

